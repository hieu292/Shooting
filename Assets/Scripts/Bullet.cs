using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    Rigidbody2D _rb;
    AudioSource _aus;
    public AudioClip hitSound;
    GameController _gc;
    public GameObject hitVfx;
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gc = FindObjectOfType<GameController>();
        _aus = FindObjectOfType<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        _rb.velocity = Vector2.up * speed;
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("TopZone"))
        {
            Destroy(gameObject);
        } else if (col.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            Destroy(col.gameObject);
            if (!_gc.GameOver)
            {
                _gc.ScoreIncrement();
            }
            if (hitSound && _aus)
            {
                _aus.PlayOneShot(hitSound);
            }
            if (hitVfx)
            {
                Instantiate(hitVfx, transform.position, Quaternion.identity);
            }
        }
    }
}
