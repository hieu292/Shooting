using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeed;
    public GameObject bullet;
    public Transform shootingPoint;
    GameController _gc;

    public AudioSource aus;
    public AudioClip shootingSound;
    
    // Start is called before the first frame update
    void Start()
    {
        _gc = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
        
        float xDir = Input.GetAxisRaw("Horizontal");
        if ((xDir > 0 && transform.position.x > 15.38f) || (xDir < 0 && transform.position.x < -5.46f))
        {
            return;
        }
        transform.position += Vector3.right * (xDir * moveSpeed * Time.deltaTime);

        
    }

    public void Shoot()
    {
        if (bullet && shootingPoint)
        {
            if (aus && shootingSound)
            {
                aus.PlayOneShot(shootingSound);
            }
            Instantiate(bullet, shootingPoint.position, Quaternion.identity);
        }
        
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            _gc.GameOver = true;
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            _gc.GameOver = true;
            Destroy(gameObject);
            Destroy(col.gameObject);
        }
    }
}
