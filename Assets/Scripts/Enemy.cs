using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    Rigidbody2D _rb;
    GameController _gc;
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gc = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        _rb.velocity = Vector2.down * speed;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("DeathZone"))
        {
            _gc.GameOver = true;
            Destroy(gameObject);
        }
    }
}
