using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject enemy;
    public float spawnTime;
    float _spawnTime;
    int _score;
    bool _isGameOver;

    public void ScoreIncrement()
    {
        _score++;
    }

    public bool GameOver
    {
        get { return _isGameOver; }
        set { _isGameOver = value; }
    }

    public void SpawnEnemy()
    {
        if (_isGameOver)
        {
            return;
        }
        float randX = Random.Range(-10.6f, 10.56f);
        Vector2 spawnPos = new Vector2(randX, 6f);
        if (enemy)
        {
            Instantiate(enemy, spawnPos, Quaternion.identity);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _spawnTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isGameOver)
        {
            _spawnTime = 0;
            return;
        }
        _spawnTime -= Time.deltaTime;

        if (_spawnTime <= 0)
        {
            SpawnEnemy();
            _spawnTime = spawnTime;
        }
    }
}
